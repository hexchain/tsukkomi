#!/usr/bin/env python2
# vim: set fileencoding=utf-8

from __future__ import unicode_literals

from gevent import monkey; monkey.patch_all()

from bottle import request, response, run, get, post, route, debug, \
    static_file, abort, view
from gevent import queue
from hashlib import sha1
from json import dumps, loads
import bottle
import gevent
import re
import redis
import lxml.etree as etree
import time
import sys

from config import config

sys.stderr = sys.stdout

DEBUG = 1

rs = redis.Redis()
strtime = time.strftime("%s")
isrgbcolor = lambda x: bool(re.match(r'^[0-9a-f]{6}$', x, re.I))
app = bottle.default_app()

w_color = {'红': 'ff0000',
           '绿': '00ff00',
           '蓝': '0000ff',
           '青': '00ffff',
           '黑': '000000',
           '白': 'ffffff',
           '黄': 'ffff00',
           '脑残粉': 'ff00ff',
           '八云紫': 'cc00cc',
           '天依蓝': '66ccff'}

responses = {
    "ping": lambda: "_(:з」∠)_",
    "error": lambda: "有什么东西坏掉了…",
    "toolong": lambda: "太长了！ ┻━┻︵╰<(=‵Д′=)>╯︵┻━┻",
    "help": lambda: """欢迎来到%s！
直接和我说话
或访问 http://t.aoikaze.moe/
就可以吐槽啦～
输入“颜色”改变弹幕颜色
吐槽请注意文明和谐喵～
(\x3cゝω·)Kira☆~""" % config.title,
    "color": {
        "none": lambda colors: "目前没有设定颜色。\n可用颜色：" +
        "\n".join(colors) + """输入
“颜色 [空格] 颜色名称(如蓝)或RGB值(如66CCFF)”
来改变颜色。""",
        "now": lambda color, colors: "当前颜色：" + color +
        "\n可用颜色：" + " ".join(colors) + """\n输入
“颜色 [空格] 代号(如 蓝)或RGB值(如66CCFF)”
来改变颜色。""",
        "invalid": lambda: "不是有效的颜色代码"
    },
    "time": lambda: "刷弹幕的时间还没到哦～",
    "ratelimit": lambda: "刷太快了，歇一歇吧…"
}


@get('/')
@view('post')
def page_index():
        return dict(title=config.title, recentmsgs=recent_messages())


@post('/')
@view('post')
def page_post():
    try:
        text = request.forms.text.strip()
        color = request.forms.color[1:]
        if len(text) == 0:
            raise ValueError
        if len(text) > config.maxlength:
            return dict(title=config.title, info="long",
                        recentmsgs=recent_messages())
    except:
        return dict(title=config.title, recentmsgs=recent_messages())

    try:
        result = publish(text, color)
    except:
        if DEBUG:
            import traceback
            traceback.print_exc()
        return dict(title=config.title, info="fail",
                    recentmsgs=recent_messages())

    return dict(title=config.title, info=result,
                recentmsgs=recent_messages())


@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=config.static_path)


@post('/api/stream')
def api_get_stream():
    response.set_header(b"Content-Type", b"text/plain")
    if request.forms.api_key != config.api_key:
        abort(401, "Access denied")
    body = queue.Queue()
    body.put('{"control": "start"}\n')
    gevent.spawn(process_messages, body)
    gevent.spawn(keepalive, body)
    return body


@route('/api/wechat', method=['GET', 'POST'])
@view('wechat')
def api_wechat_post():
    def wechat_authenticate(signature, timestamp, nonce):
        l = [timestamp, nonce, config.token]
        l.sort()
        if sha1(''.join(l)).hexdigest() == signature:
            return True
        return False

    if not wechat_authenticate(request.params['signature'],
                               request.params['timestamp'],
                               request.params['nonce']):
        abort(400)

    if request.params.get('echostr'):
        return request.params['echostr']

    body = ''.join([line.decode('utf-8') for line in request.body])
    tree = etree.XML(body)
    msgtype = tree.xpath("MsgType")[0].text
    me = tree.xpath("ToUserName")[0].text
    user = tree.xpath("FromUserName")[0].text

    if msgtype == "event":
        event = tree.xpath("Event")[0].text
        if event == "subscribe":
            return dict(fromuser=me, touser=user,
                        time=strtime(), text=responses['help']())

    elif msgtype == "text":
        try:
            text = tree.xpath("Content")[0].text.strip()

            textgroups = re.split(r"(?<!\\) ", text)
            command, args = textgroups[0], textgroups[1:]

            if command == "ping" and not args:
                return dict(fromuser=me, touser=user,
                            time=strtime(), text=responses['ping']())

            if command == "help" and not args:
                return dict(fromuser=me, touser=user,
                            time=strtime(), text=responses['help']())

            if command == "颜色":
                current_color = wechat_user_get_color(user)
                if not args:
                    if not current_color:
                        return dict(fromuser=me, touser=user, time=strtime(),
                                    text=responses['color']['none'](
                                        w_color.keys()))
                    else:
                        return dict(fromuser=me, touser=user, time=strtime(),
                                    text=responses['color']['now'](
                                        current_color,
                                        w_color.keys()))
                else:
                    if args[0] in w_color:
                        set_color = w_color[args[0]]

                    elif isrgbcolor(args[0]):
                        set_color = args[0]

                    else:
                        return dict(fromuser=me, touser=user, time=strtime(),
                                    text=responses['color']['invalid']())

                    wechat_user_set_color(user, set_color)
                    return dict(fromuser=me, touser=user, time=strtime(),
                                text=responses['color']['now'](set_color,
                                                               w_color.keys()))

            if len(text) > config.maxlength:
                return dict(fromuser=me, touser=user, time=strtime(),
                            text=responses['toolong']())

            if rs.exists("user:%s:lock" % user):
                return dict(fromuser=me, touser=user, time=strtime(),
                            text=response['ratelimit']())
            rs.psetex("user:%s:lock" % user, config.wait, 1)
            return dict(fromuser=me, touser=user, time=strtime(),
                        text=publish(text, wechat_user_get_color(user)))

        except:
            if DEBUG:
                import traceback
                traceback.print_exc()
            return dict(fromuser=me, touser=user, time=strtime(),
                        text=responses['error']())


def publish(text, color):
    if color is None:
        color = 'ffffff'

    blacklist = rs.smembers('blacklist')
    for word in blacklist:
        if word in text:
            return "success"

    data = dumps({'text': text, 'color': color, 'time': strtime()}) + '\n'
    rs.lpush('msgs', data)
    rs.publish(config.channel, data)
    return "success"


def recent_messages(num=5):
    items = [loads(i)['text'] for i in rs.lrange('msgs', 0, num-1)]
    return items


def wechat_user_set_color(user, color):
    rs.hset("user:%s" % user, 'color', color)


def wechat_user_get_color(user):
    return rs.hget("user:%s" % user, 'color')


def keepalive(body):
    while True:
        gevent.sleep(config.keepalive_interval)
        body.put('\n')


def process_messages(body):
    client = rs.pubsub()
    client.subscribe(config.channel)
    messages = client.listen()
    message = messages.next()

    while True:
        message = messages.next()
        if not message:
            continue
        data = message['data'].decode('utf-8').strip()
        try:
            body.put(data + '\n')
        except:
            import traceback
            traceback.print_exc()
            return

if __name__ == '__main__':
    debug(True)
    run(app, host='127.0.0.1', port=8051, server='gevent', reloader=True)
