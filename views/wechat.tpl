%from __future__ import unicode_literals
%if defined('echostr'):
	{{!echostr}}
%end
%if defined('text') and text:
<xml>
<ToUserName><![CDATA[{{!touser}}]]></ToUserName>
<FromUserName><![CDATA[{{!fromuser}}]]></FromUserName>
<CreateTime>{{time}}</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[{{!text}}]]></Content>
</xml>
%end
%if False:
<!-- vim: set noet ft=xml syn=xml: -->
%end
