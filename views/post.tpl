<!DOCTYPE html>
<html>
	<head>
		<meta charset=utf-8>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{{title}}</title>
		<link href="/static/css/bootstrap.min.css" rel="stylesheet">
		<link href="/static/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<style type="text/css">
.centerblock {
	text-align: center;
}
.centerblock h1 {
	font-size: 40px;
	line-height: 1.5;
}
.muted {
	color: #ccc;
}
.footer {
	text-align: center;
}
.form-control {
	border-radius: 2px;
}
.btn-lg {
	border-radius: 2px;
}
		</style>
	</head>
	<body>
	<div class="container">
		<div class="centerblock">
			<h1>{{title}}</h1>
			<p class="lead">吐槽请注意文明和谐～ (&lt;ゝω·)☆Kira~ </p>
			<p class="lead">微信吐槽帐号：<a href="http://weixin.qq.com/r/yHWGiUzEWnIlh13qnyDs">fyinori</a></p>
%if defined('info'):
%if info == 'success':
			<div class="alert alert-success">发送成功！</div>
%elif info == 'fail':
			<div class="alert alert-danger">坏掉了…</div>
%elif info == 'long':
			<div class="alert alert-info">不要发这么长的东西啦！</div>
%else:
			<div class="alert alert-info">{{info}}</div>
%end
%end
				<form id="main" method="POST" role="form" action="/">
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">
								<input type="text" name="color" id="colorpicker" class="form-control input-lg" value="#ffffff"></input>
							</div>
							<div class="col-md-10">
								<input type="text" name="text" class="form-control input-lg" placeholder="在这里输入吐槽"></input>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary btn-lg btn-block">吐槽！</button>
						</div>
					</div>
				</form>
%if defined('recentmsgs'):
			<h3>最近发送</h3>
			<table class="table">
%for message in recentmsgs:
				<tr><td>{{message}}</td></tr>
%end
			</table>
%end
		</div>
		<hr />
		<div class="footer muted">
			Copyright &copy; 2014 华中科技大学风蓝动漫社触手组
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="/static/js/jquery.cookie.min.js"></script>
		<script src="/static/js/bootstrap.min.js"></script>
		<script src="/static/js/bootstrap-colorpicker.min.js"></script>
		<script>
$(function(){
	var color = $.cookie('color');
	if (color) $('#colorpicker').val(color);
	$("#colorpicker").colorpicker();
	$("#main").submit(function(e){
		$.cookie('color', $('#colorpicker').val());
		if (!$("[name=text]").val().trim()) {
			$('.col-md-10').addClass('has-error');
			$("[name=text]").attr('placeholder', "至少说点什么啦…");
			e.preventDefault();
		}
		});
	});
		</script>
	</div>
	</body>
</html>
%if False:
<!-- vim: noet syn=html ft=html: -->
%end
