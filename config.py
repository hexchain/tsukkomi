# vim: set fileencoding=utf-8 :

from json import load


class _config(object):
    pass

config = _config()

with open('config.json', 'r') as f:
    data = load(f)
    for k, v in data.items():
        setattr(config, k, v)
